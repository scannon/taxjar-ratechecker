require 'dotenv/load'
require 'taxjar'

client = Taxjar::Client.new(api_key: ENV['TAXJAR_API_KEY'])

def action_chooser(client)
        puts "What would you like to do? Check Rates, View Your Tax Nexus, or Determine how much tax to collect for an item?"
        puts "Type Rates, Nexus, or Determine"
        
        print "> "
        action = $stdin.gets.chomp
        action = action.downcase

        if action == "rates"
            ratecheck(client)
        elsif action == "nexus"
            taxnexus(client) 
        elsif  action == "determine"
            determinetax(client)
        else action_chooser(client)
        end
end

def ratecheck(client)
    puts "Use this tool to check tax rates. Enter a zip code"
    print "> "

    zipcode = $stdin.gets.chomp

    rate = client.rates_for_location(zipcode)
    puts "Here's a breakdown of your tax rates."
    puts "You're in the state of: #{rate.state}"
    puts "You're in the county of: #{rate.county}"
    puts "You're in the city of: #{rate.city}"
    puts "Your total combined tax rate is: #{rate.combined_rate}"
    puts "Your tax rate in percent form is: #{rate.combined_rate * 100}%"
end

def taxnexus(client)
    nexus = client.nexus_regions
    
    puts ""
    puts "You have tax nexus in the following countries and regions."

    nexus.each do |item|
        puts ""
        puts item.country_code
        puts item.region_code
        puts ""
    end
end

def determinetax(client)
    puts "What is the price of the item you are selling?"
    print "> "
    price = $stdin.gets.chomp

    puts "How much is being charged for shipping?"
    print "> "
    shipping = $stdin.gets.chomp

    country = "US"

    puts "Where are you shipping this item?"
    puts "Please enter the zip code."
    print "> "
    zip_code = $stdin.gets.chomp

    puts "What state are you shipping this item to?"
    puts "Please enter the 2 character state code."
    print "> "
    state_code = $stdin.gets.chomp

    order = client.tax_for_order({
        :to_country => country,
        :to_zip => zip_code,
        :to_state => state_code,
        :amount => price.to_i,
        :shipping => shipping.to_i,
    })

    if order.has_nexus 
        puts "Your order total is $#{order.order_total_amount} with $#{order.taxable_amount} being taxable."
        puts "You should collect $#{order.amount_to_collect} for taxes."
    else puts "You don't have tax nexus in (#{state_code})."
    end

end

action_chooser(client)